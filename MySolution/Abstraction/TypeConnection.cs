﻿namespace MySolution.Models
{
    public enum TypeConnection
    {
        Auto,
        /// <summary>
        /// Load a storage file 
        /// </summary>
        Load,
        /// <summary>
        /// Create new storage file
        /// </summary>
        Create

    }
}