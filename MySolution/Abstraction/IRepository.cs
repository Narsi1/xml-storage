﻿using MySolution.Models;
using System;
using System.Collections.Generic;

namespace MySolution.Abstraction
{
    /// <summary>
    /// Represend an interface for control a data in storage.
    /// </summary>
    /// <typeparam name="TType">POCO class that represents stored data.</typeparam>
    /// <typeparam name="TKey">Simple type that specifies type of primary key in storage.</typeparam>
    interface IRepository<TType, in TKey> where TType : class 
    {
        /// <summary>
        /// Get all entities from the storage.
        /// </summary>
        /// <returns>List of entities or null</returns>
        IEnumerable<TType> GetAll();
        /// <summary>
        /// Get an entity with the given primary key value.
        /// </summary>
        /// <param name="id">The value of the primary key.</param>
        /// <returns>The entity or null</returns>
        TType Find(TKey id);
        /// <summary>
        /// Add a passed entity.
        /// </summary>
        /// <param name="entity">Entity to store.</param>
        /// <returns> The same entity that passed.</returns>
        TType Add(TType entity);
        /// <summary>
        /// Update an entity.
        /// </summary>
        /// <param name="entity">Entity to update</param>
        /// <returns>Updated an entity</returns>
        TType Update(TType entity);
        /// <summary>
        /// Delete an passed entity.
        /// </summary>
        /// <param name="id">Primary key value for entity to remove</param>
        /// <returns>True - if entity removed, False - if not.</returns>
        bool Delete(TKey id);
        /// <summary>
        /// Save all changes wich was making.
        /// </summary>
        /// <returns>True - if entity saved and flase - if not</returns>
        bool CommitChanges();
        /// <summary>
        /// Create a connection with th storage.
        /// </summary>
        /// <param name="path">Path to the storage</param>
        void CreateConnection(string path);
        /// <summary>
        /// Represend a properti for get an exception during some operation.
        /// </summary>
        Exception ExceptionAnOperation { get; }
        /// <summary>
        /// Get or set type connection(Load or Create) 
        /// </summary>
        TypeConnection TypeConnection { get; set; }
    }
}
