﻿using MySolution.Abstraction;
using MySolution.Implementations;
using MySolution.Models;
using MySolution.Properties;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml.Linq;

namespace MySolution.Controllers
{
    public class CountryController : Controller
    {
        private readonly IRepository<Country, Guid> _repository;
        public CountryController()
        {
            _repository = DependencyResolver.Current.GetService<XmlCountryRepository>();
            
        }
        
        public ActionResult Index()
        {
            _repository.CreateConnection(HttpContext.Server.MapPath(Settings.Default.StoragePathAndName));
            var countries = _repository.GetAll();
            if (countries == null && _repository.ExceptionAnOperation != null) throw  _repository.ExceptionAnOperation;
            return View(countries);
        }
        [HttpPost]
        public ActionResult SaveCountry(Country model)
        {
            if (model == null) return View("Error");
            _repository.CreateConnection(HttpContext.Server.MapPath(Settings.Default.StoragePathAndName));
            if (model.Id == Guid.Empty && !string.IsNullOrEmpty(model.Name))
            {
                if (_repository.Add(model) == null || !_repository.CommitChanges()) throw _repository.ExceptionAnOperation;
            }
            else if (model.Id != Guid.Empty && !string.IsNullOrEmpty(model.Name))
            {
                if (_repository.Update(model) == null || !_repository.CommitChanges()) throw _repository.ExceptionAnOperation;
            }
            else
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }
        public ActionResult CreateCountry()
        {
            ViewBag.Title = "Add new country";
            return PartialView("_AddAndEditForm", new Country());
        }
        public ActionResult EditCountry(Guid? id)
        {
            if (id==null|| id == Guid.Empty) return View("Error");
            _repository.CreateConnection(HttpContext.Server.MapPath(Settings.Default.StoragePathAndName));
            var coutry = _repository.Find(id.Value);
            if (coutry==null) return View("Error");
            ViewBag.Title = "Edit the country";
            return PartialView("_AddAndEditForm", coutry);
        }
        public ActionResult DeleteCountry(Guid? id)
        {
            if (id==null || id == Guid.Empty) return View("Error");
            _repository.CreateConnection(HttpContext.Server.MapPath(Settings.Default.StoragePathAndName));
            if (!_repository.Delete(id.Value) || !_repository.CommitChanges()) throw _repository.ExceptionAnOperation;
            return RedirectToAction("Index");

        }
        public ActionResult DetailsCountry(Guid? id)
        {
            if (id == null || id == Guid.Empty) return View("Error");
            _repository.CreateConnection(HttpContext.Server.MapPath(Settings.Default.StoragePathAndName));
            var coutry = _repository.Find(id.Value);
            if (coutry == null) return View("Error");
            return PartialView("_ShowDetails", coutry);
        }
    }
}