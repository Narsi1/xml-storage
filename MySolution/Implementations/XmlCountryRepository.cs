﻿using MySolution.Abstraction;
using MySolution.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace MySolution.Implementations
{
    class XmlCountryRepository : IRepository<Country, Guid>
    {
        private XDocument _documentXml;
        private bool _isload = false;
        private string _pathToXml;
        public Exception ExceptionAnOperation { get; private set; } = null;
        public TypeConnection TypeConnection { get; set; } = TypeConnection.Auto;
        public void CreateConnection(string pathToXml)
        {
            try
            {
                if (string.IsNullOrEmpty(pathToXml) || !File.Exists(pathToXml)) throw new ArgumentException("Occurred an error. The specified path is wrong.", "pathToXml");

                if (TypeConnection.Load == TypeConnection || TypeConnection == TypeConnection.Auto)
                {
                    _documentXml = XDocument.Load(pathToXml);
                    if (_documentXml == null) { throw new NullReferenceException("The xml file has not loaded"); }
                }
                else if (TypeConnection.Create == TypeConnection)
                {
                    _documentXml = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
                    _documentXml.Add("countries");
                    _documentXml.Save(pathToXml);

                }
                _isload = true;
                _pathToXml = pathToXml;
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;

            }
        }
        public bool Delete(Guid id)
        {

            try
            {
                if (!_isload) throw new Exception("The connection is wrong");
                if (id != Guid.Empty)
                {
                    _documentXml.Root.Elements()?.FirstOrDefault(r => r.Attribute("id").Value.Equals(id.ToString(), StringComparison.InvariantCulture)).Remove();
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
            }
            return false;
        }

        public Country Find(Guid id)
        {
            try
            {
                if (!_isload) throw new Exception("The connection is wrong");
                return _documentXml.Root.Elements()?.FirstOrDefault(r => r.Attribute("id").Value.Equals(id.ToString(), StringComparison.InvariantCulture))?.Deserialize<Country>();

            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
            }
            return null;
        }

        public IEnumerable<Country> GetAll()
        {
            try
            {
                if (!_isload) throw new Exception("The connection is wrong");
                var counryNodes = _documentXml.Root.Elements();
                if (counryNodes.Any())
                {
                   return  counryNodes.Select(r => r.Deserialize<Country>()).ToList<Country>();
                }
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
            }
            return null;
        }

        public Country Add(Country entity)
        {
            try
            {
                if (!_isload) throw new Exception("The connection is wrong");
                if (entity != null)
                {
                    entity.Id = GenerateID();
                    var xElement = entity.Serialize();
                    if (xElement != null)
                    {
                        _documentXml.Root.Add(xElement);
                        return entity;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
            }
            return null;
        }

        public Country Update(Country entity)
        {
            try
            {
                if (!_isload) throw new Exception("The connection is wrong");
                if (entity?.Id!=Guid.Empty)
                {
                    _documentXml.Root.Elements().FirstOrDefault(r => r.Attribute("id").Value.Equals(entity.Id.ToString())).ReplaceWith(entity.Serialize());
                    return entity;
                }
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
            }
            return null;
        }
        public bool CommitChanges()
        {
            try
            {
                _documentXml.Save(_pathToXml);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionAnOperation = ex;
                return false;
            }
        }
        private Guid GenerateID()
        {
            return Guid.NewGuid();
        }

    }
}
