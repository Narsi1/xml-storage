﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;

namespace MySolution.Implementations
{
    /// <summary>
    /// Represend the helper for serialize and diserialize entity/xml. 
    /// </summary>
    static class SerializationHelper
    {
        /// <summary>
        /// Serialize an entity to xml.
        /// </summary>
        /// <typeparam name="T">Type of passed entity</typeparam>
        /// <param name="obj">Passed entity</param>
        /// <returns>Serialized xml element or null</returns>
        public static XElement Serialize<T>(this T obj)
        {
            if (obj == null)
            {
                return null;
            }
            try
            {

                using (var memoryStream = new MemoryStream())
                {
                    using (TextWriter streamWriter = new StreamWriter(memoryStream))
                    {
                        var xmlSerializer = new XmlSerializer(typeof(T));
                        xmlSerializer.Serialize(streamWriter, obj);
                        return XElement.Parse(Encoding.ASCII.GetString(memoryStream.ToArray()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }
        /// <summary>
        /// Deserialize xml element to an entity.
        /// </summary>
        /// <typeparam name="T">Type of returned entity</typeparam>
        /// <param name="value">Xml element</param>
        /// <returns>Deserialized an entity or specified default type.</returns>
        public static T Deserialize<T>(this XElement value)
        {
            if (value == null)
            {
                return default(T);
            }
            try
            {
                var xmlDeserializer = new XmlSerializer(typeof(T));
                return (T)xmlDeserializer.Deserialize(value.CreateReader());

            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }


    }
}