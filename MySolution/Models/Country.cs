﻿using System;
using System.Xml.Serialization;

namespace MySolution.Models
{
    /// <summary>
    /// Represend a model of country. 
    /// </summary>
    [Serializable]
    [XmlRoot("country")]
    public class Country
    {
        /// <summary>
        /// Get or set a primary key value
        /// </summary>
        [XmlAttribute("id")] 
        public Guid Id { get; set; }
        /// <summary>
        /// Get or set a country name value.
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Get or set capital name value.
        /// </summary>
        [XmlElement("capital")]
        public string Capital { get; set; }
        /// <summary>
        /// Get or set description abaut a country. 
        /// </summary>
        [XmlElement("description")] public string Description { get; set; }

    }
}
